import threading
import time
import datetime

import telebot
import config
import api_handler

bot = telebot.TeleBot(config.token)
current_class_schedule = []
polls = {}


@bot.message_handler(commands=['start'])
def start(message):
    sent = bot.send_message(message.chat.id, 'Hello, *%s*!🖐\n'
                                             'Please enter verification code assigned to your profile from site' % message.from_user.first_name,
                            parse_mode='markdown')
    bot.register_next_step_handler(sent, process_user_id)


def process_user_id(msg):
    permission = api_handler.ApiClient().bind_tg_id(msg.chat.id, msg.text)

    if permission.get('success') == 0:
        sent = bot.send_message(msg.chat.id, "❗️You entered wrong identification code❗️\nTry again please!")
        bot.register_next_step_handler(sent, process_user_id)
    elif permission.get('success') == 1:
        if permission.get('extra') == 'this user' or not permission.get('extra'):
            bot.send_message(msg.chat.id, 'Thank you! ✅\nYou will get a poll as soon as it will be released! 🤙')
        elif permission.get('extra') == 'another user':
            sent = bot.send_message(msg.chat.id, '❗️This code is already obtained by another telegram user.❗️\n'
                                                 'Try to enter code again.')
            bot.register_next_step_handler(sent, process_user_id)


@bot.callback_query_handler(func=lambda call: call.data[:6] == 'pstart')
def start_poll(call):
    bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id,
                                  reply_markup=telebot.types.InlineKeyboardMarkup())
    course_id, class_type, poll_date = call.data.split('_')[1].split(':')
    class_poll = find_current_poll(course_id, class_type)
    launch = class_poll.start(call.message.chat.id, bot, poll_date)
    if launch == -1:
        bot.edit_message_text('This poll is already *finished*! You are late.\n'
                               '❗Please pay attention next time.❗', call.message.chat.id, call.message.message_id, parse_mode='markdown')

@bot.callback_query_handler(func=lambda call: call.data == 'decline')
def decline_poll(call):
    bot.edit_message_text('Poll has been declined. 🤔😒', call.message.chat.id, call.message.message_id,
                          reply_markup=telebot.types.InlineKeyboardMarkup())


def distribute_polls():
    global current_class_schedule
    current_class_schedule = api_handler.ApiClient().get_schedule_and_templates()

    while True:
        today = datetime.datetime.now() + datetime.timedelta(hours=3)
        now_time = str(today).split()[1][:5]
        week_day = today.weekday()
        sent_in_minute = False
        for cl in current_class_schedule:
            if cl.day == week_day and now_time == cl.start_time:
                cl.propose(bot)
                sent_in_minute = True
        if sent_in_minute:
            time.sleep(60)

        time.sleep(10)
        current_class_schedule = api_handler.ApiClient().get_schedule_and_templates()




def find_current_poll(course_id, class_type):
    for c in current_class_schedule:
        if c.course_id == course_id and c.class_type == class_type:
            return c

if __name__ == "__main__":
    t1 = threading.Thread(name="bot", target=bot.polling)
    t2 = threading.Thread(name='poll_send', target=distribute_polls)
    t1.start()
    t2.start()
