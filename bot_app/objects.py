import datetime
import json
import telebot.types as tp
import api_handler

days = {
    'monday': 0,
    'tuesday': 1,
    'wednesday': 2,
    'thursday': 3,
    'friday': 4,
    'saturday': 5,
    'sunday': 6
}

class CourseClass:
    '''
    Class to handle information about course events
    '''

    def __init__(self, course_id):
        self.course_name = ''
        self.class_type = ''
        self.start_time = ''
        self.end_time = ''
        self.week_day = ''
        self.day = ''
        self.course_id = course_id
        self.bot = None
        self.template = None
        self.current_question_idx = 0
        self.users_feedback = {}

    def __repr__(self):
        return "Course id: %s, class_type: %s, time: %s" % (self.course_id, self.class_type, self.start_time)


    def init_schedule(self, js: dict):
        '''
        Init class from specified dict
        :param js: includes keys 'start', 'end', 'day', 'type'
        :return
        '''
        self._set_start_time(js.get('start'))
        self._set_end_time(js.get('end'))
        self._set_day(js.get('day'))
        self.set_class_type(js.get('type'))


    def _set_start_time(self, s):
        self.start_time = s.replace('-', ':')

    def _set_end_time(self, s):
        self.end_time = s.replace('-', ':')

    def _set_day(self, s):
        self.day = days.get(s)
        self.week_day = s
        # self.day = days.get('monday')

    def set_class_type(self, s):
        self.class_type = s


    def propose(self, bot):
        # print('propose')
        '''
        Method to send to user a proposal to participate in poll
        Sends a message to a group of users that are enrolled for given course

        Warning: Executed automatically in needed time
        :param bot: telebot.Telebot object to interact with Telegram bot API
        :return:
        '''
        user_ids = api_handler.ApiClient().get_audience_for_course(self.course_id, self.start_time.replace(':', '-'), self.end_time.replace(':', '-'), self.week_day)
        print(user_ids)
        mp = tp.InlineKeyboardMarkup(row_width=2)
        today_date = str(datetime.datetime.today()).split()[0]
        mp.add(tp.InlineKeyboardButton(text='Sure!', callback_data='pstart_%s:%s:%s' % (self.course_id, self.class_type, today_date)),
               tp.InlineKeyboardButton(text='Not now', callback_data='decline'))
        for user_id in user_ids:
            if user_id != '0' and user_id != 0:
                bot.send_message(user_id,
                                  '🤓 New poll is available on course %s.\n Can you answer some questions about the class? 🙏\n'
                                  '*Deadline is today at %s*' % (self.course_name, self.end_time),
                                  reply_markup=mp, parse_mode='markdown')

    def start(self, user_id, bot, poll_date):
        '''
        Method to initiate poll when user accepts the proposal
        Send a message with the first question, further answers are processed
        by self.accept_and_send(msg)

        :param user_id: telegram_id of the user that accepted the proposal
        :param bot: telebot.Telebot object to interact with Telegram bot API
        :return:
        '''
        self.bot = bot
        today_date, today_time = str(datetime.datetime.today()).split()
        if today_date != poll_date:
            return -1
        end_time = datetime.datetime.strptime(self.end_time, "%H:%M")
        now_time = datetime.datetime.strptime(today_time[:5], "%H:%M")
        diff = end_time - now_time
        if diff.days < 0:
            return -1

        if self.template.questions:
            self.users_feedback[user_id] = self.template.fields
            sent = self.bot.send_message(user_id, self.template.questions[self.current_question_idx])
            self.bot.register_next_step_handler(sent, self.accept_and_send)
            return 1

    def accept_and_send(self, msg):
        '''
        Accept user answers and send next questions
        Checks if the field must be integer and asks
        user to reenter value
        When all questions are answered it commits
        given answers the server via api_handler

        Warning: method callable only by telebot library
        to accept messages after certain message sent by bot
        :param msg: telebot message object - answer from user on a sent message
        :return:
        '''
        question = self.template.questions[self.current_question_idx]
        value = self.template.fields.get(question)
        if isinstance(value, dict): # if question is IntField perform check on min, max and int
            minv, maxv = value.get('min'), value.get('max')
            try:
                user_value = int(msg.text)
                if minv <= user_value <= maxv:
                    self.users_feedback[msg.chat.id][question]['val'] = user_value
                else:
                    sent = self.bot.send_message(msg.chat.id, 'You entered a value not from given interval.\n'
                                                              'Please enter an integer from *%s to %s*. 😉' % (minv, maxv), parse_mode='markdown')
                    self.bot.register_next_step_handler(sent, self.accept_and_send)
                    return
            except ValueError:
                sent = self.bot.send_message(msg.chat.id, 'You were expected to enter integer value, but text message '
                                                          'was given instead. 🙁\n'
                                                          'Please enter an integer value again. 😉')
                self.bot.register_next_step_handler(sent, self.accept_and_send)
                return
        else:
            self.users_feedback[msg.chat.id][question] = msg.text

        if self.current_question_idx < len(self.template.questions) - 1:
            self.current_question_idx += 1
            sent = self.bot.send_message(msg.chat.id, self.template.questions[self.current_question_idx])
            self.bot.register_next_step_handler(sent, self.accept_and_send)
        else:
            self.bot.send_message(msg.chat.id, 'Thank you for providing feedback! 🤗\n'
                                               'See you, when new poll will be available! 👍')
            final_template = self.convert_answers(self.users_feedback[msg.chat.id])
            self.users_feedback.pop(msg.chat.id)

            data = api_handler.ApiClient().commit_feedback_from_user(msg.chat.id, self.course_id, final_template)
            if data.get('status'):
                # logger
                pass
            else:
                pass

    @staticmethod
    def convert_answers(user_answers):
        '''
        Static method to convert user answers to a
        proper format {'IntFields': {},  'CharFields': {}}
        :param user_answers: dict with answers questions {<question>: answer, <question>: {}}
        :return: template dict formatted
        '''
        template = {"CharFields": {}, "IntFields": {}}
        for q, a in user_answers.items():
            if isinstance(a, dict):
                template.get('IntFields').update({q: a})
            else:
                template.get('CharFields').update({q: a})
        return json.dumps(template)


class CourseTemplate:
    '''
    Template for the course poll class
    Parses json template and stores in fields
    In question there is a list of all questions
    '''
    def __init__(self):
        self.fields = {}
        self.questions = []

    def init_template(self, js: dict):
        '''
        Init template from json dict
        Dict must should contain CharFields and IntFields
        :param js: json dict of poll template
        :return:
        '''
        data = json.loads(js.get('data'))
        self.fields = data.get('CharFields')
        self.fields.update(data.get("IntFields"))
        self.questions = list(self.fields.keys())



