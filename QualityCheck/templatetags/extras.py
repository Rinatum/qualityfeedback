from django import template

register = template.Library()


@register.filter(name='to_status')
def convert_year(place):
    dict = {'0': 'Department of Education', '1': 'Bachelors, 1st year', '2': 'Bachelors, 2nd year', '3': 'Bachelors, 3rd year',
            '4': 'Bachelors, 4th year', '5': 'Masters, 1st year', '6': 'Masters, 2nd year', '7': 'Professors'}
    return dict[str(place)]
